import Vue from 'vue'
import VueRouter from 'vue-router'
import EnvLogs from "../views/env/Logs";
import Environments from "../views/Environments";
import EnvDashboard from "../views/env/Dashboard";
import EnvPlugins from "../views/env/Plugins";
import EnvConsole from "../views/env/Console";
import EnvManage from "../views/env/Manage";
import TplusServer from "../views/TplusServer";
import Projects from "../views/Projects";
import EnvWallet from "../views/env/Wallet";
import newLocalEnv from "../views/newLocalEnv";
import newRemoteEnv from "../views/newRemoteEnv";
import Login from "../views/Login";
import EnvCharts from "../views/env/Charts.vue"
import EnvAPI from "../views/env/API.vue"
import RPC from "../views/env/RPC";
import PluginInfo from "../views/env/PluginInfo";
import NewProject from "../views/projects/New";
import ProjectDetail from "../views/projects/ProjectDetail";

Vue.use(VueRouter)

const routes = [
    {
        path: '/env',
        name: 'Environments',
        component: Environments,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },
    {
        path: '/env/new/local',
        name: 'New Local Environment',
        component: newLocalEnv,
    },
    {
        path: '/env/new/remote',
        name: 'New Remote Environment',
        component: newRemoteEnv,
    },
    {
        path: '/env/:id/projects',
        name: 'Projects',
        component: Projects
    },
    {
        path: '/env/:id/projects/new',
        name: 'Projects',
        component: NewProject
    },
    {
        path: '/env/:id/projects/:pid',
        name: 'Projects',
        component: ProjectDetail
    },
    {
        path: '/env/:id',
        name: 'Environment Overview',
        component: EnvDashboard
    },
    {
        path: '/env/:id/logs',
        name: 'Logs',
        component: EnvLogs
    },
    {
        path: '/env/:id/charts',
        name: 'Metrics',
        component: EnvCharts
    },
    {
        path: '/env/:id/rpc',
        name: 'RPC Explorer',
        component: RPC
    },
    {
        path: '/env/:id/plugins',
        name: 'Plugins',
        component: EnvPlugins
    },
    {
        path: '/env/:id/pluginAPI/:eid',
        name: 'Plugins',
        component: EnvAPI
    },
    {
        path: '/env/:id/plugin/:pid',
        name: 'Plugin Info',
        component: PluginInfo
    },
    {
        path: '/env/:id/console',
        name: 'Console',
        component: EnvConsole
    },
    {
        path: '/env/:id/wallet',
        name: 'Wallet',
        component: EnvWallet
    },
    {
        path: '/env/:id/manage',
        name: 'Manage',
        component: EnvManage
    },
    {
        path: '/tplus/config',
        name: 'Config',
        component: TplusServer
    }
]

const router = new VueRouter({
    mode: 'history',
    base: "/ui",
    routes
})

export default router
