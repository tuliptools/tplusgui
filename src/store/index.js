import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

const axios = require('axios');


Vue.use(Vuex)

var unauth = function () {

}

export default new Vuex.Store({
  state: {
    token: "",
    activeEnv: {},
    page: "Environments",
  },
  unauthoirized: {},
  mutations: {
    login(state, data){
      state.token = data.Token
    },
    setActiveEnv(state, data){
      state.activeEnv = data
    },
    clearActiveEnv(state){
      state.activeEnv = {}
    },
    setPage(state, data){
      state.page = data
    },
    logout(state) {
      state.token = ""
      state.activeEnv = {}
    }
  },
  actions: {
    setUnauthorized(store,payload) {
      unauth = payload
    },
    setPage(store,payload) {
      store.commit("setPage",payload)
    },
    setActiveEnv(store,payload) {
      getHelper(this,"setActiveEnv","/env/" + payload.id,payload)
    },
    loadFile(store,payload) {
      getHelper(this,"ignore","/file/" + payload.file,payload)
    },
    clearActiveEnv(store) {
      store.commit("clearActiveEnv")
    },
    getLogs(store,payload) {
      getHelper(this,"ignore","/env/" + payload.id + "/logs/tezos/false",payload)
    },
    getPluginLogs(store,payload) {
      getHelper(this,"ignore","/plugins/" + payload.id + "/logs/" + payload.service ,payload)
    },
    getEnv(store,payload) {
      getHelper(this,"ignore","/env/" + payload.id,payload)
    },
    getWsURL(store,payload) {
      getHelper(this,"ignore","/env/" + payload.id + "/console/" + payload.console,payload)
    },
    getMetric(store,payload) {
      // url: /env/:id/metric/:source/:metric/:start/:end
      getHelper(this,"ignore","/env/" + payload.env + "/metric/"+payload.source+"/" + payload.metric + "/" + payload.start + "/" + payload.end,payload)
    },
    getAddresses(store,payload) {
      getHelper(this,"ignore","/wallet/" + payload.env + "/list",payload)
    },
    getProjects(store,payload) {
      getHelper(this,"ignore","/env/" + payload.env + "/projects",payload)
    },
    getProjectsImportable(store,payload) {
      getHelper(this,"ignore","/env/" + payload.env + "/projects_importable",payload)
    },
    getProjectFiles(store,payload) {
      getHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/files" ,payload)
    },
    getProjectFile(store,payload) {
      postHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/read" ,payload)
    },
    setProjectVars(store,payload) {
      postHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/variables" ,payload)
    },
    getProjectTasks(store,payload) {
      getHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/tasks" ,payload)
    },
    getSystemTask(store,payload) {
      getHelper(this,"ignore","/env/" + payload.env + "/tasks/" + payload.task ,payload)
    },
    getProjectVariables(store,payload) {
      getHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/variables" ,payload)
    },
    runProjectTask(store,payload) {
      getHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/run/" + payload.task ,payload)
    },
    getProjectTask(store,payload) {
      getHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/get/" + payload.task ,payload)
    },
    getProjectTaskLogs(store,payload) {
      getHelper(this,"ignore","/project/" + payload.env + "/" + payload.id + "/tasks/logs" ,payload)
    },
    osMemory(store,payload) {
      getHelper(this,"ignore","/os/memory",payload)
    },
    osCPU(store,payload) {
      getHelper(this,"ignore","/os/cpu",payload)
    },
    osProc(store,payload) {
      getHelper(this,"ignore","/os/proc",payload)
    },
    systemLogs(store,payload) {
      getHelper(this,"ignore","/system/logs/" + payload.n ,payload)
    },
    getPendingOps(store,payload) {
      getHelper(this,"ignore","/wallet/" + payload.env + "/pending",payload)
    },
    newAddress(store,payload) {
      postHelper(this,"ignore","/wallet/" + payload.env + "/new/" + payload.alias,payload)
    },
    newProject(store,payload) {
      postHelper(this,"ignore","/projects/" + payload.env + "/new",payload)
    },
    newProjectGit(store,payload) {
      postHelper(this,"ignore","/projects/" + payload.env + "/new_git",payload)
    },
    newProjectImport(store,payload) {
      postHelper(this,"ignore","/projects/" + payload.env + "/new_import",payload)
    },
    rmProject(store,payload) {
      getHelper(this,"ignore","/projects/" + payload.env + "/" + payload.id +"/rm",payload)
    },
    delegate(store,payload) {
      postHelper(this,"ignore","/wallet/" + payload.env + "/delegate/" + payload.from + "/" + payload.to,payload)
    },
    transfer(store,payload) {
      postHelper(this,"ignore","/wallet/" + payload.env + "/transfer/" + payload.from + "/" + payload.to + "/" + payload.amount,payload)
    },
    newFaucet(store,payload) {
      postHelper(this,"ignore","/wallet/" + payload.env + "/faucet/" + payload.alias,payload)
    },
    newAlias(store,payload) {
      postHelper(this,"ignore","/wallet/" + payload.env + "/alias/" + payload.alias + "/" + payload.addr ,payload)
    },
    bake(store,payload) {
      getHelper(this,"ignore","/wallet/" + payload.env + "/bake",payload)
    },
    deleteAddress(store,payload) {
      getHelper(this,"ignore","/wallet/" + payload.env + "/remove/" + payload.alias,payload)
    },
    login(store,payload) {
      postHelper(this,"login","/token",payload)
    },
    getEnvs(store,payload) {
      getHelper(this,"ignore","/env",payload)
    },
    getEnvTasks(store,payload) {
      getHelper(this,"ignore","/env/" + payload.env + "/tasks",payload)
    },
    createLocalEnv(store,payload) {
      postHelper(this,"ignore","/env/create/local",payload)
    },
    createRemoteEnv(store,payload) {
      postHelper(this,"ignore","/env/create/remote",payload)
    },
    createPlugin(store,payload) {
      postHelper(this,"ignore","/plugins/" + payload.env + "/register",payload)
    },
    stopPlugin(store,payload) {
      postHelper(this,"ignore","/plugins/" + payload.id + "/stop",payload)
    },
    getPlugin(store,payload) {
      getHelper(this,"ignore","/plugins/" + payload.id, payload)
    },
    startPlugin(store,payload) {
      postHelper(this,"ignore","/plugins/" + payload.id + "/start",payload)
    },
    rmPlugin(store,payload) {
      postHelper(this,"ignore","/plugins/" + payload.id + "/rm",payload)
    },
    restartEnv(store,payload){
      getHelper(this,"ignore","/env/" + payload.env + "/restart",payload)
    },
    startEnv(store,payload){
      getHelper(this,"ignore","/env/" + payload.env + "/start",payload)
    },
    destroyEnv(store,payload){
      getHelper(this,"ignore","/env/" + payload.env + "/destroy",payload)
    },
    stopEnv(store,payload){
      getHelper(this,"ignore","/env/" + payload.env + "/stop",payload)
    },
    logout(store,after) {
      store.commit("logout")
      after()
    }
  },
  modules: {
  },
  getters: {
    pageGetter (state) {
      return state.page
    }
  },
  plugins: [createPersistedState()],
})






var postHelper = function (store, name, url,payload) {

  var setPending = function(b) {
    if (payload.pendingTarget) {
      payload.pendingTarget.obj[payload.pendingTarget.field] = b
    }
  }

  var setResponse = function(response) {
    var setdata = response.data
    if (payload.processdata === true) {
      setdata = payload.datafunc(setdata)
    }
    if (payload.responseTarget) {
      payload.responseTarget.obj[payload.responseTarget.field] = setdata
    }
    setPending(false)
  }

  var options = {
    headers: {
      FROM: "TPLUSUI"
    }
  }

  if (store.state.token !== "") {
    options.headers.TOKEN = store.state.token
  }


  // delete old errors
  setResponse({ data: {Success: true, Error: ""}})
  setPending(true)

  axios.post(url,payload.data,options).then((response => {
    if (name !== "ignore") {
      store.commit(name,response.data)
    }
    if ( response.status >= 200 && response.status <= 399 && payload.onSuccess) {
      payload.onSuccess(response)
    }
    setPending(false)
  })).catch( error => {
    if (error.response.status === 401) {
      console.log(unauth)
      unauth()
   }
    if (error.response) {
      if (payload.onFailure) {
        payload.onFailure()
      }
      setResponse(error.response)
    } else {
      setResponse({ Success: false, Error: "API Request failed"})
    }
  })
}

var getHelper = function (store, name, url,payload) {

  var setPending = function(b) {
    if (payload.pendingTarget) {
      payload.pendingTarget.obj[payload.pendingTarget.field] = b
    }
  }

  var setResponse = function(response) {
    var setdata = response.data
    if (payload.processdata === true) {
      setdata = payload.datafunc(setdata)
    }
    if (payload.responseTarget) {
      payload.responseTarget.obj[payload.responseTarget.field] = setdata
    }
    setPending(false)

    if (payload.final !== undefined) {
      payload.final()
    }
  }

  var options = {
    headers: {
      FROM: "TPLUSUI"
    }
  }

  if (store.state.token !== "") {
    options.headers.TOKEN = store.state.token
  }


  // delete old errors
  setResponse({ data: {Success: true, Error: ""}})
  setPending(true)

  axios.get(url,options).then((response => {
    if (name !== "ignore") {
      store.commit(name,response.data)
    }
    setResponse(response)
    if ( response.status >= 200 && response.status <= 399 && payload.onSuccess) {
      payload.onSuccess(response)
    }
    setPending(false)
  })).catch( error => {
    if (error.response.status === 401) {
      console.log(unauth)
      unauth()
    }
    if (error.response) {
      if (payload.onFailure) {
        payload.onFailure()
      }
      setResponse(error.response)
    } else {
      setResponse({ Success: false, Error: "API Request failed"})
    }
  })
}