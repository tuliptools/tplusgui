import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Element from 'element-ui'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueTimers from 'vue-timers'
import VModal from 'vue-js-modal'
import JsonTree from 'vue-json-tree'

Vue.use(require('vue-moment'));
Vue.component('json-tree', JsonTree)
Vue.use(VModal, { dialog: true })
Vue.use(VueTimers)
Vue.config.productionTip = false
Vue.use(Element)
Vue.use(VueAxios, axios)
Vue.config.silent = true

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
