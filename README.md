# Tplus User Interface

## Run with tplus
This project is meant to be serverd by the tplus-server binary, to test things locally,
run this project with npm

```
npm run serve
```

and start tplus-server with the `TPLUS_UI_URL` environemnt variable, for example

```
TPLUS_UI_URL=http://localhost:8080 tplus-server run
```


### Documentation:

Current documentation for Tplus is available at [tplus.dev](https://tplus.dev)

### Join our Community

For feedback and questions, please find us @

* [Telegram](https://t.me/tuliptools)
* [Twitter](https://twitter.com/TulipToolsOU)
* [tezos-dev Slack](https://tezos-dev.slack.com/#/)

### Related Repositories:
* [Demo Projects](https://gitlab.com/tuliptools/tplusdemoprojects)
* [Tplus Main Repo](https://gitlab.com/tuliptools/tplus)
* [CLI Tool](https://gitlab.com/tuliptools/TplusCLI)
* [Plugins](https://gitlab.com/tuliptools/TplusPlugins)

Tplus is developed by [TulipTools](https://tulip.tools/)

![Tulip Logo](https://tulip.tools/wp-content/uploads/2020/06/tulip_small-2.png)
